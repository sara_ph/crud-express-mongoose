const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let UserSchema = new Schema({
  // userId: {type: Number},
  username: { type: String, unique: true },
  password: { type: String },
  firstName: { type: String },
  lastName: { type: String },
});
module.exports = mongoose.model("User", UserSchema);
