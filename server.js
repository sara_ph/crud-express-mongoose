//////* Reqires
const express = require("express");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Service = require("./Service");
const User = require("./User");
////////////////////*
//////* Express Connection
const app = express();
const expressPort = 3000;
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.listen(expressPort, () =>
  console.log(`Example app listening at http://localhost:${expressPort}`)
);
////////////////////*
//////* Mongoose Connection
mongoose.connect("mongodb://localhost:27017/test");
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
  // we're connected!
  console.log("hello mongoose");
});
////////////////////*
//////* CRUD Route User
//! Create
app.post("/users", async function (req, res) {
  req.body.password = await hashPassword(req.body.password);
  if ((await User.findOne({ username: req.body.username })) !== null) {
    return res.status(406).json({
      error_code: 406,
      error_message: "Bad Request: Username Is Duplicate",
    });
  }
  let user = await User.create(req.body);
  return res.status(201).send(user);
});
//! Read
app.get("/users", async (req, res) => {
  const users = await User.find();
  return res.send(users);
});
//! Update
app.put("/users/:id", async (req, res) => {
  let id = req.params.id;
  if (await User.findByIdAndUpdate(id, { $set: req.body })) {
    res.status(200).json({
      status_code: 200,
      status_message: "OK",
    });
  } else {
    res.status(404).json({
      error_code: 404,
      error_message: "Not Found",
    });
  }
});
//! Delete
app.delete("/users/:id", async function (req, res) {
  let id = req.params.id;
  if (await User.findByIdAndRemove(id)) {
    res.status(200).json({
      status_code: 200,
      status_message: "OK",
    });
  } else {
    res.status(404).json({
      error_code: 404,
      error_message: "Not Found",
    });
  }
});
////////////////////*
//////* CRUD Route Service
//! Create
app.post("/services", async function (req, res) {
  let newService = await Service.create(req.body);
  return res.status(201).send(newService);
});
//! Read
app.get("/services", async (req, res) => {
  const services = await Service.find();
  return res.send(services);
});
//! Update
app.put("/services/:id", async (req, res) => {
  let id = req.params.id;
  if (await Service.findByIdAndUpdate(id, { $set: req.body })) {
    res.status(200).json({
      status_code: 200,
      status_message: "OK",
    });
  } else {
    res.status(404).json({
      error_code: 404,
      error_message: "Not Found",
    });
  }
});
//! Delete
app.delete("/services/:id", async function (req, res) {
  let id = req.params.id;
  if (await Service.findByIdAndRemove(id)) {
    res.status(200).json({
      status_code: 200,
      status_message: "OK",
    });
  } else {
    res.status(404).json({
      error_code: 404,
      error_message: "Not Found",
    });
  }
});
////////////* Functions
async function hashPassword(password) {
  const saltRounds = 10;
  const hashedPassword = await new Promise((resolve, reject) => {
    bcrypt.hash(password, saltRounds, function (err, hash) {
      if (err) reject(err);
      resolve(hash);
    });
  });

  return hashedPassword;
}
